﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class ChangeModel : MonoBehaviour
{
    public ARController controller;
    public GameObject prefab;
    public ModelMenue menu;

    private void Start()
    {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(() =>
        {
            if(prefab != null && controller != null && menu != null)
            {
                controller.ModelPrefab = prefab;
                menu.closeList();
            } else
            {
                Debug.Log("error");
            }
        });
    }
}
