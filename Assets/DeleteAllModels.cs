﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleARCore;

public class DeleteAllModels : MonoBehaviour
{
    public ARController controller;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(() => 
        {
            if(controller != null && controller.ModelList != null)
            {
                
                for(int i = 0; i < controller.ModelList.Count; i++)
                {
                    List<Anchor> anchors = new List<Anchor>();
                    controller.ModelList[i].GetAllAnchors(anchors);
                    for(int j = 0; j < anchors.Count; j++)
                    {
                        anchors[j].transform.SetParent(null);
                        Destroy(anchors[j]);
                    }
                }
            }
        });
    }
}
