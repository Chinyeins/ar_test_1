﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
using GoogleARCore.Examples.Common;

#if UNITY_EDITOR
// Set up touch input propagation while using Instant Preview in the editor.
using Input = GoogleARCore.InstantPreviewInput;
#endif


public class AugemtedImageController : MonoBehaviour
{
    public Camera FirstPersonCamera;
    public GameObject DetectedPlanePrefab;
    public GameObject SampleModelPrefab;
    public GameObject FeaturePointPrefab;
    private const float modelRotation = -90.0f;
    private bool IsQuitting = false;
    private List<GameObject> AddedObjects = null;

    private void Start()
    {
        AddedObjects = new List<GameObject>();
    }


    public void Update()
    {
        updateApplicationLifeCycle();

        //if nothing was touched we are done
        Touch touch;
        if(Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
        {
            return;
        }

        //
        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon | TrackableHitFlags.FeaturePointWithSurfaceNormal;

        if(Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
        {
            if( (hit.Trackable is DetectedPlane) && 
                Vector3.Dot(FirstPersonCamera.transform.position -hit.Pose.position, hit.Pose.rotation * Vector3.up) <0)
            {
                Debug.Log("Hit at back of the current DetectedPlane");
            }
            else
            {
                GameObject prefab;
                if(hit.Trackable is FeaturePoint)
                {
                    prefab = SampleModelPrefab;
                } else
                {
                    prefab = FeaturePointPrefab;
                }

                //create model at hit Pose
                GameObject model = Instantiate(prefab, hit.Pose.position, hit.Pose.rotation);

                // Compensate for the hitPose rotation facing away from the raycast (i.e. camera).
                model.transform.Rotate(0, modelRotation, 0, Space.Self);

                // Create an anchor to allow ARCore to track the hitpoint as understanding of the physical
                // world evolves.
                Anchor anchor = hit.Trackable.CreateAnchor(hit.Pose);

                //make model a child of Anchor
                model.transform.parent = anchor.transform;

                //Instantiate object
                if(AddedObjects.Count >= 1)
                {
                   
                    //remove last entry
                } 

            }
        }

    }

    private void updateApplicationLifeCycle()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        if(Session.Status != SessionStatus.Tracking)
        {
            const int lostTrackingSleepTimeout = 15;
            Screen.sleepTimeout = lostTrackingSleepTimeout;
        } else
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        if(IsQuitting)
        {
            return;
        }

        //Quit if ARCore was unable to connect
        if(Session.Status == SessionStatus.ErrorPermissionNotGranted)
        {
            _ShowAndroidToastMessage("Camera permission is needed to run this application.");
            IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        } else if(Session.Status.IsError())
        {
            _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
            IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        }
    }




    /// <summary>
    /// Show an Android toast message.
    /// </summary>
    /// <param name="message">Message string to show in the toast.</param>
    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                    message, 0);
                toastObject.Call("show");
            }));
        }
    }


    /// <summary>
    /// Actually quit the application.
    /// </summary>
    private void _DoQuit()
    {
        Application.Quit();
    }
}
