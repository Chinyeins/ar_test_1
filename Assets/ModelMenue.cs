﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ModelMenue : MonoBehaviour
{
    public Button modelBtn;
    public GameObject ListView;
    private Boolean isOpen = false;
    public Boolean IsOpen {
        get {
            return this.ListView.activeSelf;
        }
        set {
            value = ListView.activeSelf;
        }
    }
    public ARController controller;


    private void Start()
    {
        modelBtn.onClick.AddListener(OpenMenu);
    }

    private void OpenMenu()
    {
        if (ListView.activeSelf)
        {
            //close
            ListView.SetActive(false);
        }
        else
        {
            //open
            ListView.SetActive(true);
        }

        IsOpen = ListView.activeSelf;
    }

    private void OnDestroy()
    {
        modelBtn.onClick.RemoveAllListeners();
    }

    public void closeList()
    {
        ListView.SetActive(false);
        IsOpen = false;
    }

    public void openList()
    {
        ListView.SetActive(true);
        IsOpen = true;
    }
}
